<?php
  
  class Api_model extends CI_Model {
      
      function exist_user_name($user_name) {
          
          $this->db->where('username', $user_name);
          $query = $this->db->get('ci_users');
          if ($query->num_rows() > 0) {
              return true;
          } else {
              return false;
          }
      }
      
      function exist_user_email($email) {
          
          $query = $this->db->get_where('ci_users', array('email' => $email));
          if ($query->num_rows() == 0){
                return false;
          } else {
                return true;
          }
      }
      
      function add_user($data) {
          
          $this->db->insert('ci_users', $data);
          return $this->db->insert_id();
      }
      
      public function login($data){
            $query = $this->db->get_where('ci_users', array('email' => $data['email']));
            if ($query->num_rows() == 0){
                return false;
            }
            else{
                //Compare the password attempt with the password we have stored.
                $result = $query->row_array();
                $validPassword = password_verify($data['password'], $result['password']);
                if($validPassword){
                    return $result = $query->row_array();
                }
                
            }
      }
      
      function resetPassword($data) {
          $this->db->where('email', $data['email']);
          $this->db->set('password', $data['password']);
          $this->db->update('ci_users');
          return true;
      }
      
      function searchByName($user_id, $username) {
          
          $this->db->like('username', $username);
          $this->db->where('id !=', $user_id);
          return $this->db->get('ci_users')->result_array();
      }
      
      function addFavorite($user_id, $target_id) {
          
          $query = $this->db->get_where('ci_contact', array('user_id' => $user_id, 'target_id' => $target_id));
          if ($query->num_rows() > 0){
              
              $this->db->where('user_id', $user_id);
              $this->db->where('target_id', $target_id);
              $this->db->set('is_favorite', "Yes");
              $this->db->set('updated_at', date('Y-m-d h:m:s'));
              $this->db->update('ci_contact');
              return true;                  
          } else {
              return false;
          }          
      }
      
      function upload_photo($user_id, $data) {
        
          $this->db->where('id', $user_id); 
          $this->db->update('ci_users', $data);
          return true;
      }
      
      function sendRequest($data) {
                                                  
          $this->db->insert('ci_history', $data);
          return true;           
      }
      
      function acceptRequest($user_id, $target_id, $room_id, $data) {
          
          $this->db->where('user_id', $target_id);
          $this->db->where('target_id', $user_id);           
          $this->db->where('room_id', $room_id);           
          $this->db->update('ci_history', $data);
          
          return true;
          
      }
      
      function registerToken($user_id, $token) {
          
          $this->db->where('id', $user_id);
          $this->db->set('token', $token);
          $this->db->update('ci_users');
      }
      
      function addContact($user_id, $target_id) {
          
          $query = $this->db->get_where('ci_contact', array('user_id' => $target_id, 'target_id' => $user_id));
          if ($query->num_rows() == 0){
              
              $this->db->set('target_id', $user_id);
              $this->db->set('user_id', $target_id);
              $this->db->set('status', 'Invited');
              $this->db->set('updated_at', date('Y-m-d h:m:s'));
              $this->db->insert('ci_contact');
              return true;                  
          } else {
              return false;
          }
      }
      
      function acceptInvite($user_id, $target_id) { 
                
          $query = $this->db->get_where('ci_contact', array('user_id' => $user_id, 'target_id' => $target_id, 'status' => 'Invited'));
          if ($query->num_rows() > 0){
              
              $this->db->where('user_id', $user_id);
              $this->db->where('target_id', $target_id);
              $this->db->set('status', 'Accepted');              
              $this->db->set('updated_at', date('Y-m-d h:m:s'));
              $this->db->update('ci_contact');
              
              $query2 = $this->db->get_where('ci_contact', array('user_id' => $target_id, 'target_id' => $user_id, 'status' => 'Invited'));
              
              if ($query2->num_rows() == 0) {
                  
                  $this->db->set('target_id', $user_id);
                  $this->db->set('user_id', $target_id);
                  $this->db->set('status', 'Accepted');
                  $this->db->set('updated_at', date('Y-m-d h:m:s'));
                  $this->db->insert('ci_contact');
                  
              } else {
                  
                  $this->db->where('target_id', $user_id);
                  $this->db->where('user_id', $target_id);
                  $this->db->set('status', 'Accepted');
                  $this->db->set('updated_at', date('Y-m-d h:m:s'));
                  $this->db->update('ci_contact');
                  
              }
              
              
              return true;                  
          }
      }
      
      function getContactList($user_id) {          
          
          $this->db->select('*');
          $this->db->from('ci_contact');
          $this->db->join('ci_users', 'ci_contact.target_id = ci_users.id');
          $this->db->where('ci_contact.user_id', $user_id);
          $this->db->where('ci_contact.is_favorite', 'No');
          
          
          $query1 = $this->db->get();                        
          $result1 = $query1->result_array(); 
          /*
          //received invitation users
          $this->db->select('*');
          $this->db->from('ci_contact');
          $this->db->join('ci_users', 'ci_contact.user_id = ci_users.id');
          $this->db->where('ci_contact.target_id', $user_id);         
          $this->db->where('ci_contact.status', 'Invited');
          $query2 = $this->db->get(); 
          
          $result2 = $query2->result_array();
          
          return array_merge($result1, $result2); 
          */
          
          return $result1;          
          
      }
      
      function getFavoriteList($user_id) {
          
          $this->db->select('*');
          $this->db->from('ci_contact');
          $this->db->join('ci_users', 'ci_contact.target_id = ci_users.id');
          $this->db->where('ci_contact.user_id', $user_id);
          $this->db->where('ci_contact.is_favorite', 'Yes');
          
          
          $query = $this->db->get();                        
          return $query->result_array();
          
      }
      
      function addBlock($user_id, $target_id) {
          
          $query = $this->db->get_where('ci_block', array('user_id' => $user_id, 
                                                          'target_id' => $target_id, 
                                                          ));
          if ($query->num_rows() == 0){
              
              $this->db->set('user_id', $user_id);
              $this->db->set('target_id', $target_id);
              $this->db->set('status', 'blocked');
              $this->db->set('created_at', date('Y-m-d h:m:s'));
              $this->db->insert('ci_block');
              return true;                  
          } else {
              $this->db->where('user_id', $user_id);
              $this->db->where('target_id', $target_id);
              $this->db->set('status', 'blocked');
              $this->db->update('ci_block');
          }
      }
      
      function getBlockList($user_id) {
          
          $this->db->select('*');
          $this->db->from('ci_block');
          $this->db->join('ci_users', 'ci_block.target_id = ci_users.id');
          $this->db->where('ci_block.user_id', $user_id);          
          $this->db->where('ci_block.status', 'blocked');          
          
          $query = $this->db->get();                        
          return $query->result_array();
          
      }
      
      function getUserById($user_id) {
          
          $query = $this->db->get_where('ci_users', array('id' => $user_id));
          
          if ($query->num_rows() > 0) {
              return $query->row_array();
          } else {
              return false;
          }
      }
      
      function unBlockUser($user_id, $target_id) {
          
          $query = $this->db->get_where('ci_block', array('user_id' => $user_id, 
                                                          'target_id' => $target_id, 
                                                          'status' => 'blocked'));
          if ($query->num_rows() > 0){
              
              $this->db->where('user_id', $user_id);
              $this->db->where('target_id', $target_id);
              $this->db->set('status', 'unblocked');              
              $this->db->update('ci_block');
              return true;                  
          } else {
              return false;
          }
      }
      
      function removeFavorite($user_id, $target_id) {
          
          $query = $this->db->get_where('ci_contact', array('user_id' => $user_id, 'target_id' => $target_id));
          if ($query->num_rows() > 0){
              
              $this->db->where('user_id', $user_id);
              $this->db->where('target_id', $target_id);
              $this->db->set('is_favorite', "No");
              $this->db->set('updated_at', date('Y-m-d h:m:s'));
              $this->db->update('ci_contact');
              return true;                  
          } else {
              return false;
          }          
      }
      
      function getToken($user_id) {
          
          return $this->db->where('id', $user_id)->get('ci_users')->row()->token;
      }
      
      function set_code($email, $code) {
          
          $this->db->where('email', $email);
          $this->db->set('code', $code);
          $this->db->update('ci_users');
      }
      
      function check_code($email, $code) {
          
          $this->db->where('email', $email);
          $this->db->where('code', $code);
          $query = $this->db->get('ci_users');
          
          if ($query->num_rows() > 0) {
              return true;
          } else {
              return false;
          }
      }
      

      
  }
?>
