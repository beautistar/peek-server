<link rel="stylesheet" href="<?= base_url() ?>public/custom_css/upload_button.css">
<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-body with-border">
        <div class="col-md-6">
          <h3><i class="fa fa-money"></i> &nbsp; Photo Editing Price Setting</h3>
        </div>        
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="box border-top-solid">
        
        <!-- /.box-header -->
        <!-- form start -->
        <div class="box-body my-form-body">
          <?php if(isset($msg) || validation_errors() !== ''): ?>
              <div class="alert alert-warning alert-dismissible">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                  <h4><i class="icon fa fa-warning"></i> Alert!</h4>
                  <?= validation_errors();?>
                  <?= isset($msg)? $msg: ''; ?>
              </div>
            <?php endif; ?>
           
            <?php echo form_open_multipart(base_url('admin/settings/add_price/'), 'class="form-horizontal"');  ?> 
              
              <div class="form-group">
                <label for="cur_price" class="col-sm-3 control-label">Current Price($)</label>

                <div class="col-sm-9">
                  <input type="number" name="cur_price" disabled class="form-control" id="cur_price" placeholder="" value=<?= $cur_price['price']; ?> >
                </div>
              </div>
              
              <div class="form-group">
                <label for="cur_service_fee" class="col-sm-3 control-label">Current Service fee($)</label>

                <div class="col-sm-9">
                  <input type="number" name="cur_service_fee" disabled class="form-control" id="cur_service_fee" placeholder="" value=<?= $cur_price['service_fee']; ?> >
                </div>
              </div>
              
              <div class="form-group">
                <label for="created_at" class="col-sm-3 control-label">Updated date</label>

                <div class="col-sm-9">
                  <input type="text" name="created_at" disabled class="form-control" id="created_at" placeholder="" value=<?= $cur_price['created_at']; ?> >
                </div>
              </div>
              
              <div class="form-group">
                <label for="price" class="col-sm-3 control-label">Photo Price($)</label>

                <div class="col-sm-9">
                  <input type="number" step="any" name="price" class="form-control" id="price" placeholder="">
                </div>
              </div>
              
              <div class="form-group">
                <label for="service_fee" class="col-sm-3 control-label">Servicce fee($)</label>

                <div class="col-sm-9">
                  <input type="number" step="any" name="service_fee" class="form-control" id="service_fee" placeholder="">
                </div>
              </div>                           
  
              <div class="form-group">
                <div class="col-md-11">
                  <input type="submit" name="submit" value="Setup" class="btn btn-info pull-right">
                </div>
              </div>
            <?php echo form_close( ); ?>
          </div>
          <!-- /.box-body -->
      </div>
    </div>
  </div>  

</section> 