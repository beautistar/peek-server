<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api extends CI_Controller {

    

    function __construct(){

        parent::__construct();
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('app/api_model', 'api_model');
        $this->load->library('session');     
    }
    
    /**
     * Make json response to the client with result code message
     *
     * @param p_result_code : Result code
     * @param p_result_msg : Result message
     * @param p_result : Result json object
     */

    private function doRespond($p_result_code,  $p_result){

         $p_result['result_code'] = $p_result_code;

         $this->output->set_content_type('application/json')->set_output(json_encode($p_result));
    }

    /**
     * Make json response to the client with success.
     * (result_code = 0, result_msg = "success")
     *
     * @param p_result : Result json object
     */

    private function doRespondSuccess($result){

        $result['message'] = "Success.";
        $this->doRespond(200, $result);
    }

    /**
     * Url decode.
     *
     * @param p_text : Data to decode
     *
     * @return text : Decoded text
     */

    private function doUrlDecode($p_text){

        $p_text = urldecode($p_text);
        $p_text = str_replace('&#40;', '(', $p_text);
        $p_text = str_replace('&#41;', ')', $p_text);
        $p_text = str_replace('%40', '@', $p_text);
        $p_text = str_replace('%20',' ', $p_text);
        $p_text = trim($p_text);


        return $p_text;
    }
    
    function test(){
        echo "API test is OK:)";
    }
    
    function version() {
        phpinfo();
    }
    
    /**
    * Send Firebase push notification
    * 
    * @param mixed $user_id
    * @param mixed $type
    * @param mixed $body
    * @param mixed $content
    */
    function sendPush($user_id, $type, $body, $content) {
        
        // send FCM push notification
        
        $url = "https://fcm.googleapis.com/fcm/send";
        $api_key = "AAAAPSde5aY:APA91bHgm1XpjkX3HoxLToc4KgP7qEKu4cRf2f40hVnOKM02bb_-s0yfiWTMz1K9_zJNNj5VeVQ06QmsONNruudhIeokdIyMHsSpxZYHBz-cslstDXLT6dh3ETQNQu8uEHtgCg5oJM6o";
        
        $token = "";
        $token = $this->api_model->getToken($user_id);
        
        if (strlen($token) == 0 ) {

            return;
        }
        
        /*
        if(is_array($target)){
            $fields['registration_ids'] = $target;
        } else{
            $fields['to'] = $target;
        }

        // Tpoic parameter usage
        $fields = array
                    (
                        'to'  => '/topics/alerts',
                        'notification'          => $msg
                    );
        $data = array('msgType' => $type,
                      'content' => $content);
        */
        $msg = array
                (
                    'body'     => $body,
                    'title'    => 'Peek',   
                    'badge' => 1,             
                    'sound' => 'default'/*Default sound*/
                );
                
        $data = array('msgType' => "notification",
                      'content' => $content);
                      
        $fields = array
            (
                //'registration_ids'    => $tokens,
                'to'                => $token,
                'notification'      => $msg,
                'priority'          => 'high',
                'data'              => $data
            );

        $headers = array(
            'Authorization: key=' . $api_key,
            'Content-Type: application/json'
        );

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 1);

        //@curl_exec($ch);
        
        $result['result'] = curl_exec($ch); 

        curl_close($ch); 
        
        return $result;
        
    }
    
    public function sendMail( $email, $code){              
             
            $to = $email;
            $subject = "Welcome to Peek.\n\n";
            $message = "Your verification code is : \n"
                        .$code.".\n\n" ;
                       
            $from = "Peek";
            $headers = "Mime-Version:1.0\n";
            $headers .= "Content-Type : text/html;charset=UTF-8\n";
            $headers .= "From:" . $from;           
            
            return mail($to, $subject, $message, $headers);         
    }
    
    public function makeRandomCode(){
         
         mt_srand();

         $random_code = '';

         $arr = array('1','2','3','4','5','6','7','8','9','0');

         for ($i = 0 ; $i < 6 ; $i++) {

             $index = mt_rand(0, 9);
             $random_code .= $arr[$index];
         }

         return $random_code;
     }
    
    
    /**
    * get user object from query array result
    * 
    * @param mixed $user_array query result array
    */
    function getUser($user_array) {
        
        $user_object = array('user_id' => $user_array['id'],
                             'email' => $user_array['email'],
                             'user_name' => $user_array['username'],
                             'phone_number' => $user_array['mobile_no'],
                             'photo_url' => $user_array['photo_url']
                             );
        return $user_object;
    }

    function signup() {
        
        $result = array();
        
        $user_name = $_POST['user_name'];
        $email = $_POST['email'];
        $phone_number = $_POST['phone_number'];
        $password = $_POST['password'];
        
        if ($this->api_model->exist_user_email($email)) {
             $result['message'] = "Email already exist.";
             $this->doRespond(201, $result);             
        } else {
            $data = array('username' => $user_name,
                          'email' => $email,
                          'mobile_no' => $phone_number,
                          'password' => password_hash($password, PASSWORD_BCRYPT),
                          'created_at' => date('Y-m-d h:m:s'),
                          'updated_at' => date('Y-m-d h:m:s')
                          );
            $result["user_id"] = $this->api_model->add_user($data);            
            $this->doRespondSuccess($result);  
        }
    }    
    
    function signin() {
        
        $result = array();
        
        $email = $_POST['email'];
        $password = $_POST['password'];
            
        $data = array(
            'email' => $email,
            'password' => $password
        );
        $q_result = $this->api_model->login($data);
        if ($q_result == TRUE) {
            $result['user_model'] = $this->getUser($q_result);
            $this->doRespondSuccess($result);            
        } else{
            $result['message'] = 'Invalid Email or Password!';
            $this->doRespond(202, $result);
        }        
    }
    
    function forgotPassword() {
        
        $result = array();
        
        $email = $_POST['email'];
        $code = $this->makeRandomCode();
        if ($this->api_model->exist_user_email($email)) {
            $this->api_model->set_code($email, $code);
            $this->sendMail($email, $code);
            $result['message'] = "User exist, Please reset new password.";
            $this->doRespondSuccess($result);
        } else {
            $result['message'] = "Invalid Email.";
            $this->doRespond(203, $result);
        }
    }
    
    function resetPassword() {
        
        $result = array();
        
        $email = $_POST['email'];
        $password = $_POST['password'];
        $code = $_POST['code'];
        
        if ($this->api_model->check_code($email, $code) == false) {
            $result['message'] = "Invalid code.";
            $this->doRespond(203, $result);
        } else {
            $data = array('email' => $email,                          
                          'password' => password_hash($password, PASSWORD_BCRYPT)                           
                          );
            $this->api_model->resetPassword($data); 
            $this->doRespondSuccess($result);
        }
    }
    
    function searchByName() {
        
        $result = array();
        $user_result = array();
        
        $user_id = $_POST['user_id'];
        $username = $_POST['user_name'];
        
        $q_result = $this->api_model->searchByName($user_id, $username);
        
        if (count($q_result) == 0) {
            $result['message'] = "No data.";
            $result['user_list'] = $user_result;
            $this->doRespond(204, $result);
        } else {
            
            foreach ($q_result as $row) {
                $user = $this->getUser($row);
                array_push($user_result, $user);
            }
            $result['user_list'] = $user_result;            
            $this->doRespondSuccess($result);            
        }                                    
    }
    
    function addFavorite() {
        
        $result = array();
        $user_id = $_POST['user_id'];
        $target_id = $_POST['target_id'];
        
        if ($this->api_model->addFavorite($user_id, $target_id)) {  
            $this->doRespondSuccess($result);
        } else {
            $result['message'] = "Already added.";
            $this->doRespond(205, $result);
        }
    }
    
    function removeFavorite() {
        
        $result = array();
        $user_id = $_POST['user_id'];
        $target_id = $_POST['target_id'];
        
        if ($this->api_model->removeFavorite($user_id, $target_id)) {  
            $this->doRespondSuccess($result);
        } else {
            $result['message'] = "Already removed.";
            $this->doRespond(205, $result);
        }
    }
    
    function uploadPhoto() {
         
         $result = array();
         
         $user_id = $_POST['user_id'];

                  
         if(!is_dir("uploadfiles/")) {
             mkdir("uploadfiles/");
         }
         $upload_path = "uploadfiles/";  

         $cur_time = time();
         
         $dateY = date("Y", $cur_time);
         $dateM = date("m", $cur_time);
         
         if(!is_dir($upload_path."/".$dateY)){
             mkdir($upload_path."/".$dateY);
         }
         if(!is_dir($upload_path."/".$dateY."/".$dateM)){
             mkdir($upload_path."/".$dateY."/".$dateM);
         }
         
         $upload_path .= $dateY."/".$dateM."/";
         $upload_url = base_url().$upload_path;

        // Upload file. 

        $w_uploadConfig = array(
            'upload_path' => $upload_path,
            'upload_url' => $upload_url,
            'allowed_types' => "*",
            'overwrite' => TRUE,
            'max_size' => "100000KB",
            'max_width' => MAX_WIDTH,
            'max_height' => MAX_HEIGHT,
            'file_name' => $user_id.'_'.intval(microtime(true) * 10)
        );

        $this->load->library('upload', $w_uploadConfig);

        if ($this->upload->do_upload('file')) {

            $file_url = $w_uploadConfig['upload_url'].$this->upload->file_name;
            $data = array('photo_url' => $file_url, 
                          'updated_at' => date('Y-m-d h:m:s')
                          );
            $id = $this->api_model->upload_photo($user_id, $data);
            $result['upload_url'] = $file_url;            
            $this->doRespondSuccess($result);

        } else {

            $this->doRespond(206, $result);// upload fail
            return;
        }     
    }
    
    function sendRequest() {
        
        $result = array();
        
        $user_id = $_POST['user_id'];
        $target_id = $_POST['target_id'];
        $room_id = "peek_room";
        
        $data = array('user_id' => $user_id,
                      'target_id' => $target_id,
                      'room_id' => $room_id,
                      'status' => 'Requested',
                      'requested_at' => date('Y-m-d h:m:s')
                      );
        
        if ($this->api_model->sendRequest($data)) {
            
            $this->sendPush($target_id, "Request", "You received request.", "OK");
        }
        $this->doRespondSuccess($result);
    }
    
    function acceptRequest() {
        
        $result = array();
        
        $user_id = $_POST['user_id'];
        $target_id = $_POST['target_id'];
        $room_id = $_POST['room_id'];
        
        $data = array(
                      'status' => 'Acceptted',
                      'accepted_at' => date('Y-m-d h:m:s')
                      );
        
        if ($this->api_model->acceptRequest($user_id, $target_id, $room_id, $data)) {
            
            $this->sendPush($target_id, "Accepted", "Your request is accepted.", "OK");
            
        }
        $this->doRespondSuccess($result);
        
    }
    
    function registerToken() {
        
        $result = array();
        
        $user_id = $_POST['user_id'];
        $token = $_POST['token'];
        
        $this->api_model->registerToken($user_id, $token);
        
        $this->doRespondSuccess($result);
    }
    
    function sendInvite() {
        
        $result = array();
        $user_id = $_POST['user_id'];
        $target_id = $_POST['target_id'];
        
        if ($this->api_model->addContact($user_id, $target_id)) {  
            $this->doRespondSuccess($result);
        } else {
            $result['message'] = "Already sent.";
            $this->doRespond(205, $result);
        }
    }
    
    function acceptInvite() {
        
        $result = array();
        $user_id = $_POST['user_id'];
        $target_id = $_POST['target_id'];
        
        if ($this->api_model->acceptInvite($user_id, $target_id)) {  
            $this->doRespondSuccess($result);
        } else {
            $result['message'] = "Already accepted.";
            $this->doRespond(205, $result);
        }
    }
    
    function getContactList() {
        
        $result = array();
        $user_result = array();
        
        $user_id = $_POST['user_id'];
        
        $query_result = $this->api_model->getContactList($user_id);
        
        if (count($query_result) == 0) {
            
            $result['message'] = "No data.";
            $result['contact_list'] = $user_result;
            $this->doRespond(204, $result);           
            
        } else {
            foreach ($query_result as $row) {
                $user = $this->getUser($row);
                $user['status'] = $row['status'];
                array_push($user_result, $user);
            }
            $result['contact_list'] = $user_result;            
            $this->doRespondSuccess($result);
        }
        
    }
    
    function getFavoriteList() {
        
        $result = array();
        $user_result = array();
        
        $user_id = $_POST['user_id'];
        
        $query_result = $this->api_model->getFavoriteList($user_id);
        
        if (count($query_result) == 0) {
            
            $result['message'] = "No data.";
            $result['favorite_list'] = $user_result;
            $this->doRespond(204, $result);           
            
        } else {
            foreach ($query_result as $row) {
                $user = $this->getUser($row);                
                array_push($user_result, $user);
            }
            $result['favorite_list'] = $user_result;            
            $this->doRespondSuccess($result);
        }
        
    }
    
    function blockUser() {
        
        $result = array();
        $user_id = $_POST['user_id'];
        $target_id = $_POST['target_id'];
        
        if ($this->api_model->addBlock($user_id, $target_id)) {  
            $this->doRespondSuccess($result);
        } else {
            $result['message'] = "Already blocked.";
            $this->doRespond(205, $result);
        }
    }
    
    function getBlockList() {
        
        $result = array();
        $user_result = array();
        
        $user_id = $_POST['user_id'];
        
        $query_result = $this->api_model->getBlockList($user_id);
        
        if (count($query_result) == 0) {
            
            $result['message'] = "No data.";
            $result['block_list'] = $user_result;
            $this->doRespond(204, $result);           
            
        } else {
            foreach ($query_result as $row) {
                $user = $this->getUser($row);                
                array_push($user_result, $user);
            }
            $result['block_list'] = $user_result;            
            $this->doRespondSuccess($result);
        }        
    }
    
    function getUserById() {
        
        $result = array();
        
        $user_id = $_POST['user_id'];
            
        $q_result = $this->api_model->getUserById($user_id);
        if ($q_result == TRUE) {
            $result['user_model'] = $this->getUser($q_result);
            $this->doRespondSuccess($result);            
        } else{
            $result['message'] = 'User does not exist';
            $this->doRespond(202, $result);
        }
    }
    
    function unBlockUser() {
        
        $result = array();
        $user_id = $_POST['user_id'];
        $target_id = $_POST['target_id'];
        
        if ($this->api_model->unBlockUser($user_id, $target_id)) {  
            $this->doRespondSuccess($result);
        } else {
            $result['message'] = "Already unblocked.";
            $this->doRespond(205, $result);
        }
    }
    
    function sendNotification() {
        
        $result = array();
        $user_id = $_POST['user_id'];
        $target_id = $_POST['target_id'];
        
        $result = $this->sendPush($target_id, "Request", "You received notification.", "OK");
        
        $this->doRespondSuccess($result);
        
        
    }    
       
    
}
?>
