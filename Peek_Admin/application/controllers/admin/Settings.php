<?php
    defined('BASEPATH') OR exit('No direct script access allowed');

    class Settings extends MY_Controller {

        public function __construct(){
            parent::__construct();
            $this->load->model('admin/setting_model', 'setting_model');
        }

        public function index(){
            
            $data['cur_price'] = $this->setting_model->get_cur_price();
            $data['title'] = 'Setting';
            $data['view'] = 'admin/settings/price';
            $this->load->view('admin/layout', $data);
        }
        
        //---------------------------------------------------------------
        //  Add User
        public function add_price(){
            if($this->input->post('submit')){

                $this->form_validation->set_rules('price', 'Price', 'trim|required'); 
                $this->form_validation->set_rules('service_fee', 'Service fee', 'trim|required'); 

                if ($this->form_validation->run() == FALSE) {
                    $data['cur_price'] = $this->setting_model->get_cur_price();
                    $data['title'] = 'Setting';
                    $data['view'] = 'admin/settings/price';
                    $this->load->view('admin/layout', $data);
                }
                else{
                    $data = array(
                        'price' => $this->input->post('price'),
                        'service_fee' => $this->input->post('service_fee'),
                        'created_at' => date('Y-m-d h:m:s')
                    );
                    $data = $this->security->xss_clean($data);
                    $result = $this->setting_model->add_price($data);
                    if($result){
                        $this->session->set_flashdata('msg', 'Price has been updated Successfully!');
                        redirect(base_url('admin/settings'));
                    }
                }
            }
            else {
                $data['cur_price'] = $this->setting_model->get_cur_price();            
                $data['title'] = 'Setting';
                $data['view'] = 'admin/settings/price';
                $this->load->view('admin/layout', $data);
            }
            
        }
                
    }


?>